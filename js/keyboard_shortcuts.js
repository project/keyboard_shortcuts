;(function($, window, document) {

    function triggerActionCallback (selector, type, prevent_default) {
        return function () {
            $(selector).trigger(type);
            return !prevent_default;
        };
    }

    function modalActionCallback (url, prevent_default) {
        return function () {

            var ajaxSettings = {
                url: url,
                dialogType: 'modal',
                dialog: { width: 400 },
            };

            var myAjaxObject = Drupal.ajax(ajaxSettings);
            myAjaxObject.execute();

            return !prevent_default;
        };
    }

    function redirectActionCallback (url, prevent_default) {
        return function () {
            window.location = url;
            return !prevent_default;
        };
    }

    function callbackActionCallback (callback_function, prevent_default){
        return function (e, combo) {
            prevent_default &= window[callback_function](e, combo);
            return !prevent_default;
        };
    }

    Drupal.behaviors.keyboardShortcuts = {
        attach: function (context, settings) {
            if (context === document) {
                for (var index in settings.keyboard_shortcuts.shortcuts) {
                    var shortcut = settings.keyboard_shortcuts.shortcuts[index];
                    var keystrokes = $.map(shortcut.keystroke.split(/[,]+/), $.trim);

                    var action = shortcut.action;
                    var prevent_default = shortcut.prevent_default;

                    switch(action){
                        case 'trigger':

                            Mousetrap.bind(keystrokes, triggerActionCallback(
                              shortcut.arguments.selector,
                              shortcut.arguments.type,
                              prevent_default)
                            );

                            break;

                        case 'modal':

                            Mousetrap.bind(keystrokes, modalActionCallback(
                              shortcut.arguments.url,
                              prevent_default)
                            );

                            break;

                        case 'redirect':

                            Mousetrap.bind(keystrokes, redirectActionCallback(
                              shortcut.arguments.url,
                              prevent_default)
                            );

                            break;

                        case 'callback':

                            Mousetrap.bind(keystrokes, callbackActionCallback(
                              shortcut.arguments.callback,
                              prevent_default)
                            );

                            break;
                    }

                }

                Mousetrap['stopCallback'] = function(e, element, combo) {

                    // If the element has the class "mousetrap" then no need to stop.
                    if ((' ' + element.className + ' ').indexOf(' mousetrap ') > -1) {
                        return false;
                    }

                    for (var index in settings.keyboard_shortcuts.shortcuts) {

                        var shortcut = settings.keyboard_shortcuts.shortcuts[index];
                        var keystrokes = shortcut.keystroke.split(/[\s,]+/);

                        for(var s in keystrokes){
                            if(combo===keystrokes[s] && shortcut.enable_during_input){
                                return false;
                            }
                        }
                    }

                    // Stop for input, select, and textarea.
                    return element.tagName === 'INPUT' ||
                      element.tagName === 'SELECT' ||
                      element.tagName === 'TEXTAREA' ||
                      (element.contentEditable &&
                       element.contentEditable === 'true');
                };
            }
        },

    };

})(jQuery, window, document);
