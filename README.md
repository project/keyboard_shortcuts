# INTRODUCTION

Keyboard shortcuts allows you to create keyboard shortcuts on your website. Shortcuts can be configured to:
* perform an action (click) on an element (using css selectors)
* redirect to a url
* open a url in a modal
* call a custom javascript callback


Similar modules:
* keyboard_shortcuts (D7 only module)
* hotkey (D5-6 only module)

Works with GIN admin theme (see recommended modules)

# REQUIREMENTS

Keyboard shortcuts module requires the Mousetrap js library (http://craig.is/killing/mice) to recognise the keyboard sequences. Download the latest version of Mousetrap library from github (https://github.com/ccampbell/mousetrap/archive/master.zip) and unzip its contents into /libraries/mousetrap.

# RECOMMENDED MODULES

- none

# INSTALLATION

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

# CONFIGURATION

## General usage

After installing the module, 3 shortcuts will be available by default.
* ctrl+n or command+n : opens /node/add in a modal.
* ctrl+s or command+s : saves a form (#edit-submit buttons).
* ctrl+e or command+e : opens the edit form of the current node (if available).

Configuration is found under the standard "User Interface" configuration item.
/admin/config/user-interface/keyboard_shortcuts

# MAINTAINERS

Current maintainers:
* Kris Booghmans (kriboogh) - https://www.drupal.org/u/kriboogh

This project has been sponsored by:
* Calibrate
  In the past fifteen years, we have gained large expertise in
  consulting organizations in various industries.
  https://www.calibrate.be
